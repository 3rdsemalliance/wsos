/*  This is a very early version of how I imagine the system would work.
 *  In principle there will be a relational database comprised of a few tables:
 *  1. Personal information database
 *     Used to store information like the soldier's name and surname, as well as ID,
 *     so that the ID can be later used to unambiguously identify a certain soldier.
 *     Fields: NAME, SURNAME, STAFF_ID, RANK
 *  2. Team database
 *     Used to store information about every soldier's assigned team.
 *     Teams do not apply to generals.
       Fields: TEAM_ID, TEAM_LEADER_ID, TEAM_MEMBER_[I] (how many members in one team do we want at most?)
 *  3. Army database
 *     Used to store information about every team's assigned army.
 *     Fields: TEAM_ID, ARMY_ID
 *  4. Meeting/timeline database
 *     Used to store information about meetings, as well as times and people assigned to them.
 *     Fields: MEETING_ID, MEETING_SCOPE(either entire army, army team leaders or just team), ARMY_ID, TEAM_ID, TIME
 *     IMPORTANT QUESTION: Do we want this to be more dynamic? Say, have a maximum of any X assignees to any meeting?
 *  5. Role database/login database
 *     Used to store login information for identity verification purposes, 
 *     as well as store the staff members's role/clearance level, to know which class to assign to the session.
 *     Fields: ID, USER_NAME, USER_PASS, CLEARANCE
 * 
 */


#include <iostream>
#include <sqlite3.h>
#include <vector>

using namespace std;
class Staff{
    private:
        string name;
        string surname;
        string ID;
    public:
        Staff(){
            name = "testowy";
            surname = "tescik";
            ID = "S001";
        }
        string get_name(){
            return name;
        }
        string get_surname(){
            return surname;
        }
        string get_ID(){
            return ID;
        }
};

class Soldier : public Staff{
    private:
        string rank;
        int clearance_level;
    public:
        string get_rank(){
            return rank;
        }
        int get_clearance_level(){
            return clearance_level;
        }
};

class Team_Leader;
class General;

class Private : public Soldier{
    private:
        string unit_ID;
    public:
        Private(){
            unit_ID = "T123";
        }
        string get_unit_ID(){
            return unit_ID;
        }
        string get_unit_leader(){
            //this function will get and return the unit leader's name, surname, rank and ID
            //(We assume a regular soldier should not have access to more information)
            return "placeholder";
        }
        void check_schedule(){
            //this function will check the meeting database for any meetings that have been assigned
            //to the soldier or to his team, and will display them
        }
};

class Team_Leader: public Soldier{
    private:
        string unit_ID;
        string army_ID;
    public:
        Team_Leader(){
            //placeholder
        }
        string get_supervising_general_ID(){
            //this function will return the name, surname and ID of the team's supervising general
            //this can either be done by either storing the information in advance as a class member
            //or be found dynamically whenever needed in the database
            return "placeholder";
        }
        vector<Private> get_team(){
            //this function will search the database for every soldier assigned to the leader's team
            //and return their information in an array
            vector<Private> team;
            return team; //placeholder function
        }
        Private get_soldier_info(string soldier_ID){
            //this function will check whether the soldier is on his team or not, and if it's true,
            //returns all of his information
            Private desired;
            return desired; 
        }
        void assign_meeting(){
            //this function will write a meeting into the meeting database
            //which info can then be accessed by team members
        }
};

class General : public Soldier{
    private:
        string army_ID;
    public:
        string get_army_ID(){
            return army_ID;
        }
        void create_team(string team_ID, Team_Leader new_team_leader){
            //this function will create a team of given ID in the general's army,
            //assign a team leader to it and write it to the team database.
            //important: this will have to check whether a reassignment is happening
            //and accordingly notify the general that the soldier is already leading a team
            //so that there's no leader in two teams at once
        }
        void list_teams(){
            //this function will list all teams in the general's army
            //(maybe also list team leaders?)
        }
        void assign_to_team(string team_ID, string ID){
            //this function will assign a given soldier to a given team
            //important: this will have to check whether a reassignment is happening
            //and accordingly remove the soldier from his current team, so that
            //there's no soldier in two teams at once.
        }
        Soldier get_soldier_info(string ID){
            //this function will check if a soldier is a part of the general's army,
            //then return his info if the test returns true
            Soldier desired;
            return desired;
        }
        void disband_team(string team_ID){
            //this function will remove every soldier from a given team
            //and set their team to "Unassigned",
            //as well as remove the team from the army/team table
        }
        void change_leader(string team_ID, string ID){
            //this function will change a team's leader from the current leader
            //to the soldier specified by the ID, and set the current leader's
            //team to "Unassigned"
            //Important: This has to check whether a reassignment is happening,
            //and accordingly notify the general that the soldier is already a
            //team leader somewhere else
        }
        void assign_meeting(){
            //this function will write a meeting into the meeting database,
            //for a given time, for given assignees
            //this is different from the Team_Leader function in its scope:
            //the General must be able to assign meetings for everyone in his
            //army, whereas the Team_Leader - only for his subordinates.
            //It would make no sense for a Team_Leader to be able to set his
            //General's schedule.
        }
};