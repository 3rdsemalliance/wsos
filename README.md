# WSOS - Wojskowy System Obsługi Sołdatów

WSOS (czyt. Wu-sos) to prosty i szybki (w zamyśle) system do zarządzania bazą wojskową (a przynajmniej jej uproszczonym modelem). Będzie oferował on podstawową funkcjonalność do przechowywania danych personelu, a także tworzenia oddziałów, jednostek, zarządzania personelem i komunikacją wewnętrzną. Readme będzie aktualizowane w miarę postępów. Tak, nazwa jest nawiązaniem.

Po skończeniu prac zamierzamy opublikować projekt na licencji GNU GPL v3.
Jeśli znajdziesz błąd lub wymyślisz lepsze rozwiązanie - pisz!
